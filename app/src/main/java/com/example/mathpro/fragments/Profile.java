package com.example.mathpro.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.mathpro.R;
import com.example.mathpro.activities.PaymentActivity;
import com.example.mathpro.activities.SplashScreen;
import com.example.mathpro.activities.accounts.SettingsActivity;
import com.example.mathpro.activities.cards.MyCreditCardListActivity;
import com.example.mathpro.activities.register.PersonalDataActivity;
import com.example.mathpro.interfaces.CustomAlertInterface;
import com.example.mathpro.models.UserDetailsResponse;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.CustomAlert;
import com.example.mathpro.utils.MyUtils;
import com.example.mathpro.utils.PreferenceHandler;
import com.example.mathpro.webservice.ApiInterface;
import com.example.mathpro.webservice.HttpRequest;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile extends Fragment {

    private View rootView = null;
    private Context context;
    private LinearLayout linearEdit, linearRateOurSerivce, linearMyCard, linearSettings, linearLogout;
    private TextView txtTopUp, txtName, txtEmail, txtPhone, txtUniversity, txtCreditBalance;
    private ProgressBar progressBar;
    private ImageView imgPhoto;
    private String userId = "";

    public Profile() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        }
        init(rootView);

        return rootView;
    }

    private void init(View view) {

        context = getActivity();
        linearEdit = view.findViewById(R.id.editProfileLinear);
        linearRateOurSerivce = view.findViewById(R.id.fp_rateOurServiceLinear);
        txtTopUp = view.findViewById(R.id.fp_txtTopUp);
        linearMyCard = view.findViewById(R.id.fp_myCreditCardLinear);
        linearSettings = view.findViewById(R.id.fp_SettingsLinear);
        linearLogout = view.findViewById(R.id.fp_linearLogout);
        txtCreditBalance = view.findViewById(R.id.fp_txtCredit);
        progressBar = view.findViewById(R.id.fp_progressBar);

        imgPhoto = view.findViewById(R.id.fp_profileImageView);
        txtName = view.findViewById(R.id.fp_userNameTxt);
        txtEmail = view.findViewById(R.id.fp_userEmailTxt);
        txtPhone = view.findViewById(R.id.fp_phoneTxt);
        txtUniversity = view.findViewById(R.id.fp_universityTxt);

        userId = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_ID);

        listeners();

    }

    private void listeners() {


        txtTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PaymentActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

        linearRateOurSerivce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RateOurServiceDialog ratingDialog = new RateOurServiceDialog(getActivity());
                ratingDialog.show(getActivity().getSupportFragmentManager(), "dialog");

            }
        });

        linearMyCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), MyCreditCardListActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

        linearSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

        linearLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogOutAlert();
            }
        });

    }

    private void getAccountDetails() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<UserDetailsResponse> call = apiService.getAccountDetails(Constants.API_KEY, userId);
            call.enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        UserDetailsResponse userDetailsResponse = response.body();

                        if (userDetailsResponse != null) {
                            String status = userDetailsResponse.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateDetails(userDetailsResponse);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateDetails(UserDetailsResponse userDetailsResponse) {

        ArrayList<UserDetailsResponse.Datum> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.addAll(userDetailsResponse.getData());
        String name = userDetailsResponse.getData().get(0).getName();
        String ic = userDetailsResponse.getData().get(0).getIc();
        String email = userDetailsResponse.getData().get(0).getEmail();
        String number = userDetailsResponse.getData().get(0).getContactNo();
        String university = userDetailsResponse.getData().get(0).getUniversity();
        String imgUrl = userDetailsResponse.getData().get(0).getProfilePicture();
        String creditBalance = userDetailsResponse.getData().get(0).getTopUpBalance();
        String gender = userDetailsResponse.getData().get(0).getGenderId();

        if (MyUtils.checkStringValue(name)) {
            txtName.setText(name);
        }
        if (MyUtils.checkStringValue(email)) {

            txtEmail.setText(email);
        }

        if (MyUtils.checkStringValue(number)) {

            txtPhone.setText(number);
        }

        if (MyUtils.checkStringValue(university)) {

            txtUniversity.setText(university);
        }

        if (MyUtils.checkStringValue(imgUrl)) {

            loadImageSource(imgUrl);
        }


        if (MyUtils.checkStringValue(creditBalance)) {
            txtCreditBalance.setText(creditBalance);
        }
        linearEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PersonalDataActivity.class);
                intent.putExtra(Constants.IS_FROM, Constants.IS_FROM_ACCOUNT_EDIT);
                intent.putExtra("image", imgUrl);
                intent.putExtra("name", name);
                intent.putExtra("ic", ic);
                intent.putExtra("email", email);
                intent.putExtra("university", university);
                intent.putExtra("gender", gender);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());

            }
        });

    }

    private void showLogOutAlert() {
        CustomAlert customAlert = new CustomAlert(context, "You are sure want to logout?", "LOGOUT", "CANCEL", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                PreferenceHandler.clearPreferences(context);
                Intent intent = new Intent(context, SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
        customAlert.showDialog();
    }

    private void loadImageSource(String profileUrl) {
        Glide.with(context).load(profileUrl).placeholder(R.drawable.account_profile).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(imgPhoto);
    }

    @Override
    public void onResume() {
        super.onResume();
        getAccountDetails();
    }
}
