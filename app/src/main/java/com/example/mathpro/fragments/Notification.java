package com.example.mathpro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mathpro.R;
import com.example.mathpro.adapters.NotificationItemAdapter;
import com.example.mathpro.interfaces.OnItemClicked;
import com.example.mathpro.models.NotificationListModel;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.MyUtils;
import com.example.mathpro.utils.PreferenceHandler;
import com.example.mathpro.webservice.ApiInterface;
import com.example.mathpro.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notification extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private NotificationItemAdapter notificationItemAdapter = null;
    private View rootView = null;
    private Context context;
    private ProgressBar progressBar, bottomProgressBar;
    private boolean isLoading = true;
    private int pageNumber = 0;
    private String userId = "";

    ArrayList<NotificationListModel.Datum> arrayList = new ArrayList<>();

    public Notification() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        }
        init(rootView);

        return rootView;
    }

    private void init(View view) {

        recyclerView = view.findViewById(R.id.fn_recyclerView);

        progressBar = view.findViewById(R.id.fh_progressBar);
        bottomProgressBar = view.findViewById(R.id.fh_bottomProgressBar);
        context = getActivity();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

    }

    private void getRecentQuestions(int number, final boolean isLoadingForFirstTime) {
        try {
            if (isLoadingForFirstTime) {
                isLoading = true;
                pageNumber = 0;
                arrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<NotificationListModel> call = apiService.getNotification(Constants.API_KEY,String.valueOf(pageNumber),userId);
            call.enqueue(new Callback<NotificationListModel>() {
                @Override
                public void onResponse(Call<NotificationListModel> call, Response<NotificationListModel> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    if (response.isSuccessful()) {
                        NotificationListModel notificationListModel = response.body();

                        if (notificationListModel != null) {
                            String status = notificationListModel.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                adaptToRecycler(notificationListModel);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<NotificationListModel> call, Throwable t) {
                    hideProgressBar(isLoadingForFirstTime);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar(isLoadingForFirstTime);
            if (!isLoadingForFirstTime) {
                isLoading = isLoadingForFirstTime;
            }
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    private void adaptToRecycler(NotificationListModel notificationListModel) {

        arrayList.addAll(notificationListModel.getData());

        if (arrayList.size() > 0) {

            if (notificationItemAdapter != null) {
                notificationItemAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {
                notificationItemAdapter = new NotificationItemAdapter(context, arrayList);

                notificationItemAdapter.setOnItemClicked(new OnItemClicked() {
                    @Override
                    public void onItemClicked(int position) {

                    }
                });

                recyclerView.setAdapter(notificationItemAdapter);
            }

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                pageNumber = pageNumber + 1;

                                getRecentQuestions(pageNumber, false);

                                Log.e("recyclerView", "bottom_reached");
                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getRecentQuestions(0, true);
    }
}
