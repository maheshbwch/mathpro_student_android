package com.example.mathpro.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.mathpro.R;
import com.example.mathpro.activities.FAQsDetailsActivity;
import com.example.mathpro.utils.MyUtils;

public class FAQs extends Fragment {

    private CardView cardFreq, cardCredits;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_faq, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {

        cardFreq = view.findViewById(R.id.cardFreq);
        cardCredits = view.findViewById(R.id.cardCredits);

        listeners();
    }

    private void listeners() {


        cardFreq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), FAQsDetailsActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

        cardCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), FAQsDetailsActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });


    }
}
