package com.example.mathpro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mathpro.R;
import com.example.mathpro.activities.AddPostActivity;
import com.example.mathpro.activities.AnswersDetailActivity;
import com.example.mathpro.activities.PaymentActivity;
import com.example.mathpro.adapters.RecentQuesHomeAdapter;
import com.example.mathpro.interfaces.OnItemClicked;
import com.example.mathpro.models.RecentQuestionsModel;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.MyUtils;
import com.example.mathpro.utils.PreferenceHandler;
import com.example.mathpro.webservice.ApiInterface;
import com.example.mathpro.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends Fragment {

    private Context context;
    private View rootView = null;
    private LinearLayout addPostLinear;
    private ProgressBar progressBar, bottomProgressBar;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecentQuesHomeAdapter recentQuesHomeAdapter = null;
    private String userId = "";
    private String topUpBalance = "";
    private double balance_int = 0.00;
    private TextView txtTopUp, txtCreditBalance;
    private boolean isLoading = true;
    private int pageNumber = 0;
    private double minumumLockAmount = 49.00;

    ArrayList<RecentQuestionsModel.Datum> arrayList = new ArrayList<>();

    public Home() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home, container, false);
        }
        init(rootView);

        return rootView;

    }

    private void init(View view) {


        recyclerView = view.findViewById(R.id.fh_recyclerView);
        addPostLinear = view.findViewById(R.id.addPostLinear);
        txtTopUp = view.findViewById(R.id.fh_txtTopUp);
        txtCreditBalance = view.findViewById(R.id.fh_creditBalance);
        progressBar = view.findViewById(R.id.fh_progressBar);
        bottomProgressBar = view.findViewById(R.id.fh_bottomProgressBar);
        context = getActivity();


        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        userId = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_ID);

        listeners();
    }

    private void listeners() {

        addPostLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyUtils.checkStringValue(topUpBalance)) {
                    if (topUpBalance.contains(" ")) {
                        String[] splitWords = topUpBalance.split("\\s+");
                        balance_int = Double.parseDouble(splitWords[0]);
                        if (balance_int > minumumLockAmount) {
                            Intent intent = new Intent(getActivity(), AddPostActivity.class);
                            startActivity(intent);
                            MyUtils.openOverrideAnimation(true, getActivity());
                        } else {
                            MyUtils.showSnackBar(v, context, "Your credit balance is too low please topUp to continue!");
                        }
                    }
                }
            }
        });

        txtTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PaymentActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

    }

    private void getRecentQuestions(int number, final boolean isLoadingForFirstTime) {
        try {
            if (isLoadingForFirstTime) {
                isLoading = true;
                pageNumber = 0;
                arrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<RecentQuestionsModel> call = apiService.getRecentQuestion(Constants.API_KEY, userId, Constants.APP_ID, String.valueOf(pageNumber));
            call.enqueue(new Callback<RecentQuestionsModel>() {
                @Override
                public void onResponse(Call<RecentQuestionsModel> call, Response<RecentQuestionsModel> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    if (response.isSuccessful()) {
                        RecentQuestionsModel recentQuestionsModel = response.body();

                        if (recentQuestionsModel != null) {
                            String status = recentQuestionsModel.getStatus();
                            topUpBalance = recentQuestionsModel.getTopUpBalance();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                adaptToRecycler(recentQuestionsModel);
                            }
                            if (MyUtils.checkStringValue(String.valueOf(topUpBalance))) {
                                txtCreditBalance.setText(topUpBalance);
                            }
                        } else {
                            Log.e("error", "response un successful");
                        }

                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<RecentQuestionsModel> call, Throwable t) {
                    hideProgressBar(isLoadingForFirstTime);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar(isLoadingForFirstTime);
            if (!isLoadingForFirstTime) {
                isLoading = isLoadingForFirstTime;
            }
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    private void adaptToRecycler(RecentQuestionsModel latestNewsModel) {

        arrayList.addAll(latestNewsModel.getData());

        if (arrayList.size() > 0) {

            if (recentQuesHomeAdapter != null) {
                recentQuesHomeAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {
                recentQuesHomeAdapter = new RecentQuesHomeAdapter(context, arrayList);
                recentQuesHomeAdapter.setOnItemClicked(new OnItemClicked() {
                    @Override
                    public void onItemClicked(int position) {

                        String questionId = arrayList.get(position).getQuestionId();
                        String status = arrayList.get(position).getStatus();

                        Intent intent = new Intent(context, AnswersDetailActivity.class);
                        intent.putExtra(Constants.QUESTIONS_ID, questionId);
                        intent.putExtra(Constants.STATUS_QA, status);
                        context.startActivity(intent);
                        MyUtils.openOverrideAnimation(true, getActivity());
                    }
                });
                recyclerView.setAdapter(recentQuesHomeAdapter);
            }

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                pageNumber = pageNumber + 1;

                                getRecentQuestions(pageNumber, false);

                                Log.e("recyclerView", "bottom_reached");
                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getRecentQuestions(0, true);
    }
}
