package com.example.mathpro.interfaces;

public interface OnItemClicked {
    void onItemClicked(int position);
}
