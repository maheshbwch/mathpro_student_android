package com.example.mathpro.interfaces;

public interface DialogInterface {
    void onButtonClicked();
    void onDismissedClicked();
}
