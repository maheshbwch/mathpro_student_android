package com.example.mathpro.interfaces;

public interface ConnectivityInterface {
    void onConnected();
    void onNotConnected();
}
