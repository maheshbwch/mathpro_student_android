package com.example.mathpro.interfaces;


import com.example.mathpro.models.CountryCodes;

public interface OnSelectedListener {

    void onSelected(CountryCodes.Datum countryCodes);
    void onNothingSelected();

}