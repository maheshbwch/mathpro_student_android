package com.example.mathpro.interfaces;

public interface UploadAlertListener {
    void onChooseFileClicked();
    void onCaptureCameraClicked();
}
