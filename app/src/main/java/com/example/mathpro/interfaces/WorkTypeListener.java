package com.example.mathpro.interfaces;


import com.example.mathpro.models.WorkTypes;

public interface WorkTypeListener {
    void onItemClicked(WorkTypes workTypes);
}
