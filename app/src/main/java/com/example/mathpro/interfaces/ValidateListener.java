package com.example.mathpro.interfaces;

public interface ValidateListener {
    void onValidated();
}
