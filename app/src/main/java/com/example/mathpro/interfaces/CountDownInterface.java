package com.example.mathpro.interfaces;

public interface CountDownInterface {
    void onTicking(long millisUntilFinished);
    void onFinished();

}
