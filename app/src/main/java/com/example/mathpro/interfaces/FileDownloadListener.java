package com.example.mathpro.interfaces;

public interface FileDownloadListener {

    void onDownloadSuccess(String filePath);
    void onDownloadFailed(String errorMessage);

}
