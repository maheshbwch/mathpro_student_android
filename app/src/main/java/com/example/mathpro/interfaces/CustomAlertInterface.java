package com.example.mathpro.interfaces;

public interface CustomAlertInterface {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
