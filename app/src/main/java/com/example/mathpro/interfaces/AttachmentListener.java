package com.example.mathpro.interfaces;

public interface AttachmentListener {
    void onImageRemoved(int position);
    void onImageChanged(int position);
}
