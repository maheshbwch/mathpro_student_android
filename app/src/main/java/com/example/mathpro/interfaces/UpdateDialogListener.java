package com.example.mathpro.interfaces;

public interface UpdateDialogListener {
    void onUpdateButtonClicked();

    void onNotNowButtonClicked();

    void onDismissButtonClicked();

}
