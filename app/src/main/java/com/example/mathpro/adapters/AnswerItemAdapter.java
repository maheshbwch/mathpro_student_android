package com.example.mathpro.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.mathpro.R;
import com.example.mathpro.activities.AnswersDetailActivity;

import java.util.ArrayList;

public class AnswerItemAdapter extends RecyclerView.Adapter<AnswerItemAdapter.ViewHolder> {

    private ArrayList<String> arrayList;
    private Context context;


    public AnswerItemAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_my_answer_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

       /* boolean isSelected = arrayList.get(position).isSelected();
        holder.itemView.setSelected(isSelected);

        String id = arrayList.get(position).getId();
        String name = arrayList.get(position).getName();
*/
       /* if (id.equalsIgnoreCase(Constants.ASCENDING) || id.equalsIgnoreCase(Constants.DESCENDING)) {
            holder.dateImage.setVisibility(View.VISIBLE);

            if (id.equalsIgnoreCase(Constants.ASCENDING)) {
                holder.dateImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_down));
            } else {
                holder.dateImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_up));
            }

        } else {
            holder.dateImage.setVisibility(View.GONE);
        }*/

    }

    @Override
    public int getItemCount() {
        //return arrayList.size();
        return 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtSeeAns;
        public ImageView dateImage;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtSeeAns = itemView.findViewById(R.id.ima_txtSeeAns);
            //this.dateImage = itemView.findViewById(R.id.ifl_dateImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* String id = arrayList.get(getAdapterPosition()).getId();

                    if (id.equalsIgnoreCase(Constants.ASCENDING) || id.equalsIgnoreCase(Constants.DESCENDING)) {
                        if (id.equalsIgnoreCase(Constants.ASCENDING)) {
                            arrayList.get(getAdapterPosition()).setId(Constants.DESCENDING);
                        } else {
                            arrayList.get(getAdapterPosition()).setId(Constants.ASCENDING);
                        }
                    }
                    selectItem(getAdapterPosition());*/
                }
            });

            txtSeeAns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, AnswersDetailActivity.class);
                    context.startActivity(intent);
                }
            });


        }
    }
}

