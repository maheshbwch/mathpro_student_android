package com.example.mathpro.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.mathpro.R;
import com.example.mathpro.interfaces.OnItemClicked;
import com.example.mathpro.models.RecentQuestionsModel;
import com.example.mathpro.utils.MyUtils;

import java.util.ArrayList;

public class RecentQuesHomeAdapter extends RecyclerView.Adapter<RecentQuesHomeAdapter.ViewHolder> {

    private ArrayList<RecentQuestionsModel.Datum> arrayList;
    private Context context;
    private OnItemClicked onItemClicked = null;


    public RecentQuesHomeAdapter(Context context, ArrayList<RecentQuestionsModel.Datum> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_recent_home_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String txtTitle = arrayList.get(position).getTopicDetails();
        String txtDesc = arrayList.get(position).getQuestionDetails();
        String txtPrice = arrayList.get(position).getQuotation();
        String txtStatus = arrayList.get(position).getStatus();
        String txtUrgent = arrayList.get(position).getIsUrgent();
        String txtTimeTaken = arrayList.get(position).getTimeTaken();
        String txtDate = arrayList.get(position).getCreatedAt();

        if (MyUtils.checkStringValue(txtTitle)) {
            holder.txtTitle.setText(txtTitle);
        }

        if (MyUtils.checkStringValue(txtDesc)) {
            holder.txtDesc.setText(txtDesc);
        }

        if (MyUtils.checkStringValue(txtPrice)) {
            if (txtPrice.equalsIgnoreCase("--")) {
                holder.txtPrice.setText("0.00 MYR");
            } else {
                holder.txtPrice.setText(txtPrice);
            }
        }
        if (MyUtils.checkStringValue(txtStatus)) {

            if (txtStatus.equalsIgnoreCase("Answered")) {
                //holder.linearSeeAll.setVisibility(View.VISIBLE);
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.green));
            } else {
               // holder.linearSeeAll.setVisibility(View.GONE);
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
            }
            holder.txtStatus.setText(txtStatus);
        }
        if (MyUtils.checkStringValue(txtUrgent)) {
            if (txtUrgent.equalsIgnoreCase("1")) {
                holder.txtIsurgent.setText("URGENT");
            } else {
                holder.txtIsurgent.setText("");
            }
        }

        if (MyUtils.checkStringValue(txtTimeTaken)) {
            holder.txtTimeTaken.setText(txtTimeTaken);

        }

        if (MyUtils.checkStringValue(txtDate)) {
            holder.txtDate.setText(txtDate);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle,txtDesc, txtPrice, txtStatus, txtIsurgent, txtTimeTaken, txtDate;
        public LinearLayout linearSeeAll;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.irh_txtTitle);
            this.txtDesc = itemView.findViewById(R.id.irh_txtDesc);
            this.txtPrice = itemView.findViewById(R.id.irh_txtPrice);
            this.txtStatus = itemView.findViewById(R.id.irh_txtAnswerStatus);
            this.txtIsurgent = itemView.findViewById(R.id.irh_txtStatus);
            this.txtTimeTaken = itemView.findViewById(R.id.irh_txtTime);
            this.txtDate = itemView.findViewById(R.id.irh_txtDate);
            this.linearSeeAll = itemView.findViewById(R.id.linearSeeAnswer);

           /*linearSeeAll.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   //onItemClicked.onItemClicked(getAdapterPosition());

               }
           });*/

           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   onItemClicked.onItemClicked(getAdapterPosition());
               }
           });
        }
    }


}
