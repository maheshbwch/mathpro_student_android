package com.example.mathpro.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.mathpro.fragments.FAQs;
import com.example.mathpro.fragments.History;
import com.example.mathpro.fragments.Home;
import com.example.mathpro.fragments.Notification;
import com.example.mathpro.fragments.Profile;


public class HomePagerAdapter extends FragmentPagerAdapter {

    private History history = new History();

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Home(); //ChildFragment1 at position 0
            case 1:
                return new FAQs(); //ChildFragment2 at position 1
            case 2:
                return history; //ChildFragment3 at position 3
            case 3:
                return new Notification(); //ChildFragment3 at position 2
            case 4:
                return new Profile(); //ChildFragment4 at position 4
        }
        return null; //does not happen
    }

    public void setFilterListener(String selectedDate,String serviceType){
        history.getFilterValues(selectedDate,serviceType);
    }



    @Override
    public int getCount() {
        return 5; //four fragments
    }
}
