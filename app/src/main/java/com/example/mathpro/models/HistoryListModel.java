package com.example.mathpro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HistoryListModel extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("question_id")
        @Expose
        private String questionId;
        @SerializedName("is_top_up_row")
        @Expose
        private String is_top_up_row;

        @SerializedName("questioner_name")
        @Expose
        private String questionerName;
        @SerializedName("profile_picture")
        @Expose
        private String profilePicture;
        @SerializedName("app_name")
        @Expose
        private String appName;
        @SerializedName("chapter_name")
        @Expose
        private String chapterName;
        @SerializedName("chapter_id")
        @Expose
        private String chapterId;
        @SerializedName("topic_name")
        @Expose
        private String topicName;
        @SerializedName("topic_id")
        @Expose
        private String topicId;
        @SerializedName("topic_details")
        @Expose
        private String topicDetails;
        @SerializedName("question_details")
        @Expose
        private String questionDetails;
        @SerializedName("quotation")
        @Expose
        private String quotation;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("is_urgent")
        @Expose
        private String isUrgent;
        @SerializedName("time_taken")
        @Expose
        private String timeTaken;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("top_up_balance")
        @Expose
        private String topUpBalance;



        public String getIs_top_up_row() {
            return is_top_up_row;
        }

        public void setIs_top_up_row(String is_top_up_row) {
            this.is_top_up_row = is_top_up_row;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getQuestionerName() {
            return questionerName;
        }

        public void setQuestionerName(String questionerName) {
            this.questionerName = questionerName;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterId() {
            return chapterId;
        }

        public void setChapterId(String chapterId) {
            this.chapterId = chapterId;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public String getTopicId() {
            return topicId;
        }

        public void setTopicId(String topicId) {
            this.topicId = topicId;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getQuestionDetails() {
            return questionDetails;
        }

        public void setQuestionDetails(String questionDetails) {
            this.questionDetails = questionDetails;
        }

        public String getQuotation() {
            return quotation;
        }

        public void setQuotation(String quotation) {
            this.quotation = quotation;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsUrgent() {
            return isUrgent;
        }

        public void setIsUrgent(String isUrgent) {
            this.isUrgent = isUrgent;
        }

        public String getTimeTaken() {
            return timeTaken;
        }

        public void setTimeTaken(String timeTaken) {
            this.timeTaken = timeTaken;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getTopUpBalance() {
            return topUpBalance;
        }

        public void setTopUpBalance(String topUpBalance) {
            this.topUpBalance = topUpBalance;
        }

    }
}
