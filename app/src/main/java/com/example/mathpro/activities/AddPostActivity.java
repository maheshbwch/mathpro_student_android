package com.example.mathpro.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mathpro.R;
import com.example.mathpro.adapters.CertificateListAdapter;
import com.example.mathpro.interfaces.AttachmentListener;
import com.example.mathpro.interfaces.UploadAlertListener;
import com.example.mathpro.models.BaseModel;
import com.example.mathpro.models.Certificates;
import com.example.mathpro.models.ChapterResponse;
import com.example.mathpro.models.TopicResponse;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.MyUtils;
import com.example.mathpro.utils.PermissionAlert;
import com.example.mathpro.utils.PermissionRequest;
import com.example.mathpro.utils.PreferenceHandler;
import com.example.mathpro.utils.RealPathUtil;
import com.example.mathpro.utils.UploadAlert;
import com.example.mathpro.utils.custom_spinner.OnItemSelected;
import com.example.mathpro.utils.custom_spinner.SpinnerDialog;
import com.example.mathpro.webservice.ApiInterface;
import com.example.mathpro.webservice.HttpRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPostActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack, chapterImageView, topicImageView;
    private LinearLayout selectChapterLinear, selectTopicLinear, linearAddAttachment;
    private TextView txtVisibleChapter, txtVisibleTopic, txtGoneChapter, txtGoneTopic, txtPost;
    private EditText edtQuestions;
    private RecyclerView recyclerViewAttachment;
    private CheckBox checkIsUrgent;
    private CertificateListAdapter certificateListAdapter = null;
    private ArrayList<Certificates> imagePathArrayList = new ArrayList<>();
    private ProgressDialog progressDialog = null;


    private int FILES_CODE = Constants.TAKE_IMAGE_FROM_FILES;
    private int CAMERA_CODE = Constants.TAKE_IMAGE_FROM_CAMERA;

    private String imageFilePath = "";
    private static final int IMAGE_LIMIT = 5;

    private String attachmentFile1 = "";
    private String attachmentFile2 = "";
    private String attachmentFile3 = "";
    private String attachmentFile4 = "";
    private String attachmentFile5 = "";
    private String chapterId = "", topicId = "";
    private String userId = "", isUrgent = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        context = AddPostActivity.this;
        init();
    }

    private void init() {

        imgBack = findViewById(R.id.imgBack);
        selectChapterLinear = findViewById(R.id.asb_selectChapterLinear);
        selectTopicLinear = findViewById(R.id.asb_selectTopicLinear);
        chapterImageView = findViewById(R.id.asb_chapterImage);
        topicImageView = findViewById(R.id.asb_TopicImage);
        checkIsUrgent = findViewById(R.id.aap_isUrgent);

        txtVisibleChapter = findViewById(R.id.asb_selectChapterTxt);
        txtGoneChapter = findViewById(R.id.asb_chapterNameTxt);

        txtVisibleTopic = findViewById(R.id.asb_selectTopicTxt);
        txtGoneTopic = findViewById(R.id.asb_topicNameTxt);

        recyclerViewAttachment = findViewById(R.id.aap_attachmentFile);
        linearAddAttachment = findViewById(R.id.linearAddAttachment);

        txtPost = findViewById(R.id.aap_txtPost);

        edtQuestions = findViewById(R.id.aap_edtQuestions);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);


        listener();

    }

    private void listener() {


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        linearAddAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePathArrayList.size() < IMAGE_LIMIT) {

                    FILES_CODE = Constants.TAKE_IMAGE_FROM_FILES;
                    CAMERA_CODE = Constants.TAKE_IMAGE_FROM_CAMERA;

                    showUploadAlert();
                } else {
                    MyUtils.showSnackBar(v, context, "Attachment limit reached");
                    Log.e("limit", "Image limit reached");
                }
            }
        });

        txtPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                attachmentFile1 = "";
                attachmentFile2 = "";
                attachmentFile3 = "";
                attachmentFile4 = "";
                attachmentFile5 = "";

                String questions = edtQuestions.getText().toString();

                if (checkIsUrgent.isChecked()) {
                    isUrgent = "1";
                } else {
                    isUrgent = "0";
                }
                if (!MyUtils.checkStringValue(questions)) {
                    MyUtils.showSnackBar(v, context, "The Question field is required");
                } else if (!MyUtils.checkStringValue(chapterId)) {
                    MyUtils.showSnackBar(v, context, "Please choose Chapter before post");
                } else if (!MyUtils.checkStringValue(topicId)) {
                    MyUtils.showSnackBar(v, context, "Please choose Topic before post");
                } else if (!(imagePathArrayList.size() >0)) {
                    MyUtils.showSnackBar(v, context, "Please add at least one attachment before post");
                } else {
                    if (MyUtils.isConnected(context)) {
                        getPathNames(v);
                    } else {
                        MyUtils.showSnackBar(v, context, String.valueOf(R.string.internet_failed));
                    }
                }
            }
        });


        getChapterListValues();

        emptyAdaptRecyclerView();
    }

    private void getPathNames(View v) {
        switch (imagePathArrayList.size()) {
            case 1:
                attachmentFile1 = "" + imagePathArrayList.get(0).getImageFilePath();
                attachmentFile2 = "";
                attachmentFile3 = "";
                attachmentFile4 = "";
                attachmentFile5 = "";
                break;
            case 2:
                attachmentFile1 = "" + imagePathArrayList.get(0).getImageFilePath();
                attachmentFile2 = "" + imagePathArrayList.get(1).getImageFilePath();
                attachmentFile3 = "";
                attachmentFile4 = "";
                attachmentFile5 = "";
                break;
            case 3:
                attachmentFile1 = "" + imagePathArrayList.get(0).getImageFilePath();
                attachmentFile2 = "" + imagePathArrayList.get(1).getImageFilePath();
                attachmentFile3 = "" + imagePathArrayList.get(2).getImageFilePath();
                attachmentFile4 = "";
                attachmentFile5 = "";
                break;
            case 4:
                attachmentFile1 = "" + imagePathArrayList.get(0).getImageFilePath();
                attachmentFile2 = "" + imagePathArrayList.get(1).getImageFilePath();
                attachmentFile3 = "" + imagePathArrayList.get(2).getImageFilePath();
                attachmentFile4 = "" + imagePathArrayList.get(3).getImageFilePath();
                attachmentFile5 = "";
                break;
            case 5:
                attachmentFile1 = "" + imagePathArrayList.get(0).getImageFilePath();
                attachmentFile2 = "" + imagePathArrayList.get(1).getImageFilePath();
                attachmentFile3 = "" + imagePathArrayList.get(2).getImageFilePath();
                attachmentFile4 = "" + imagePathArrayList.get(3).getImageFilePath();
                attachmentFile5 = "" + imagePathArrayList.get(4).getImageFilePath();
                break;
            default:
                attachmentFile1 = "";
                attachmentFile2 = "";
                attachmentFile3 = "";
                attachmentFile4 = "";
                attachmentFile5 = "";
                break;
        }


        Log.e("attachmentFile1", ":" + attachmentFile1);
        Log.e("attachmentFile2", ":" + attachmentFile2);
        Log.e("attachmentFile3", ":" + attachmentFile3);
        Log.e("attachmentFile4", ":" + attachmentFile4);
        Log.e("attachmentFile5", ":" + attachmentFile5);

        postQuestionToApi(v);

    }

    private void postQuestionToApi(View v) {
        try {

            String question = edtQuestions.getText().toString();

            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Constants.API_KEY));
            map.put("student_id", RequestBody.create(MediaType.parse("text/plain"), userId));
            map.put("app_id", RequestBody.create(MediaType.parse("text/plain"), Constants.APP_ID));
            map.put("chapter_id", RequestBody.create(MediaType.parse("text/plain"), chapterId));
            map.put("topic_id", RequestBody.create(MediaType.parse("text/plain"), topicId));
            map.put("details", RequestBody.create(MediaType.parse("text/plain"), question));
            map.put("is_urgent", RequestBody.create(MediaType.parse("text/plain"), isUrgent));
            Log.e("is_urgent", "" + isUrgent);

            progressDialog = MyUtils.showProgressLoader(context, "Posting...");

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.postQuestion(map, getFileBody("attachment_file[0]", attachmentFile1),
                    getFileBody("attachment_file[1]", attachmentFile2),
                    getFileBody("attachment_file[2]", attachmentFile3), getFileBody("attachment_file[3]", attachmentFile4),
                    getFileBody("attachment_file[4]", attachmentFile5));
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        BaseModel baseModel = response.body();
                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String error = baseModel.getMessage();
                            Log.e("status", ": " + status);
                            if (status.equalsIgnoreCase(Constants.SUCCESS)) {
                                MyUtils.showLongToast(context, "Successfully Posted");
                                finish();
                            } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                MyUtils.showSnackBar(v, context, MyUtils.checkStringValue(error) ? error : "Unable to post question try again..");
                            }
                        } else {
                            MyUtils.showSnackBar(v, context, "Unable to post question try again..");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    MyUtils.showSnackBar(v, context, "Unable to post question try again..");
                    Log.e("error_message", ": " + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
            MyUtils.showSnackBar(v, context, "Unable to post question try again..");
        }
    }


    private MultipartBody.Part getFileBody(String keyParam, String attachmentFile) {
        MultipartBody.Part body = null;
        if (MyUtils.checkStringValue(attachmentFile)) {
            File file = new File(attachmentFile);
            RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
            body = MultipartBody.Part.createFormData(keyParam, file.getName(), requestFile);
        } else {
            body = MultipartBody.Part.createFormData(keyParam, "");
        }
        return body;
    }

    private void emptyAdaptRecyclerView() {
        imagePathArrayList.clear();
        recyclerViewAttachment.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewAttachment.setHasFixedSize(true);

        certificateListAdapter = new CertificateListAdapter(context, imagePathArrayList);
        certificateListAdapter.setOnItemClicked(new AttachmentListener() {
            @Override
            public void onImageRemoved(int position) {
                removeImage(position);
            }

            @Override
            public void onImageChanged(int position) {

                FILES_CODE = Constants.CHANGE_IMAGE_FROM_FILES;
                CAMERA_CODE = Constants.CHANGE_IMAGE_FROM_CAMERA;
                showUploadAlert();
            }
        });
        recyclerViewAttachment.setAdapter(certificateListAdapter);
    }

    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, AddPostActivity.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, AddPostActivity.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        imageFilePath = null;
        MyUtils.intentToImageSelection(this, FILES_CODE);
    }

    private void intentToCameraApp() {
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(this, CAMERA_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case Constants.TAKE_IMAGE_FROM_FILES:
                        imageFilePath = RealPathUtil.getPath(context, data.getData());
                        Log.e("selectedFile", "path:" + imageFilePath);
                        if (MyUtils.checkStringValue(imageFilePath)) {
                            adaptImageToRecycler(imageFilePath);
                        }
                        break;
                    case Constants.TAKE_IMAGE_FROM_CAMERA:
                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            adaptImageToRecycler(imageFilePath);
                        }
                        break;
                    default:
                        break;

                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    private void adaptImageToRecycler(String imageFilePath) {
        setImage(imageFilePath);
    }

    private void setImage(String imageFilePath) {
        imagePathArrayList.add(new Certificates(imageFilePath));
        if (certificateListAdapter != null) {
            certificateListAdapter.notifyDataSetChanged();
        }
    }

    private void removeImage(int position) {
        imagePathArrayList.remove(position);
        if (certificateListAdapter != null) {
            certificateListAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddPostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(AddPostActivity.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


    private void getChapterListValues() {
        try {
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ChapterResponse> call = apiService.getChapterList(Constants.API_KEY, Constants.APP_ID);
            call.enqueue(new Callback<ChapterResponse>() {
                @Override
                public void onResponse(Call<ChapterResponse> call, Response<ChapterResponse> response) {

                    if (response.isSuccessful()) {
                        ChapterResponse chapterResponse = response.body();
                        if (chapterResponse != null) {
                            loadChapterList(chapterResponse);
                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ChapterResponse> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void loadChapterList(ChapterResponse chapterResponse) {
        final ArrayList<ChapterResponse.Datum> chapterArrayList = new ArrayList<>();
        chapterArrayList.clear();
        chapterArrayList.addAll(chapterResponse.getData());

        if (chapterArrayList.size() > 0) {
            selectChapterLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SpinnerDialog spinnerDialog = new SpinnerDialog(chapterArrayList, new OnItemSelected() {
                        @Override
                        public void onItemSelected(int position) {
                            String chapterNo = chapterArrayList.get(position).getChapterNo();
                            String chapterName = chapterArrayList.get(position).getChapterName();
                            chapterId = chapterArrayList.get(position).getChapterId();
                            updateChapterInUI(chapterNo, chapterName, chapterId);
                        }
                    }, false);
                    spinnerDialog.setTitle("Select Year/Form");
                    spinnerDialog.show(getSupportFragmentManager(), "dialog");
                }
            });
        }

    }

    private void updateChapterInUI(String chapterNo, String chapterName, String chapterId) {
        if (txtVisibleChapter.getVisibility() == View.VISIBLE) {
            txtVisibleChapter.setVisibility(View.GONE);
        }
        if (txtGoneChapter.getVisibility() == View.GONE) {
            txtGoneChapter.setVisibility(View.VISIBLE);
        }
        txtGoneChapter.setText("Chapter" + chapterNo + ": " + chapterName);

        if (MyUtils.checkStringValue(chapterId)) {
            getTopicList(chapterId);
        }

    }

    private void getTopicList(String chapterId) {
        try {
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<TopicResponse> call = apiService.getTopicList(Constants.API_KEY, chapterId);
            call.enqueue(new Callback<TopicResponse>() {
                @Override
                public void onResponse(Call<TopicResponse> call, Response<TopicResponse> response) {
                    if (response.isSuccessful()) {
                        TopicResponse topicResponse = response.body();
                        if (topicResponse != null) {
                            loadTopicList(topicResponse);
                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<TopicResponse> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void loadTopicList(TopicResponse topicResponse) {
        final ArrayList<TopicResponse.Datum> topicArrayList = new ArrayList<>();
        topicArrayList.clear();
        topicArrayList.addAll(topicResponse.getData());

        if (topicArrayList.size() > 0) {
            selectTopicLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SpinnerDialog spinnerDialog = new SpinnerDialog(topicArrayList, new OnItemSelected() {
                        @Override
                        public void onItemSelected(int position) {
                            String topicName = topicArrayList.get(position).getTopicName();
                            topicId = topicArrayList.get(position).getTopicId();
                            updateTopicInUI(topicName, topicId);
                        }
                    }, false);
                    spinnerDialog.setTitle("Select Topic");
                    spinnerDialog.show(getSupportFragmentManager(), "dialog");
                }
            });
        }

    }

    private void updateTopicInUI(String topicName, String topicId) {
        if (txtVisibleTopic.getVisibility() == View.VISIBLE) {
            txtVisibleTopic.setVisibility(View.GONE);
        }
        if (txtGoneTopic.getVisibility() == View.GONE) {
            txtGoneTopic.setVisibility(View.VISIBLE);
        }
        txtGoneTopic.setText("Topic: " + topicName);

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddPostActivity.this);
    }
}
