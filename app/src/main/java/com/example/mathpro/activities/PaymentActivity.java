package com.example.mathpro.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mathpro.R;
import com.example.mathpro.interfaces.CustomAlertInterface;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.CustomAlert;
import com.example.mathpro.utils.MyUtils;
import com.example.mathpro.utils.PreferenceHandler;


public class PaymentActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack;
    private WebView webView;
    private ProgressBar progressBar;
    private String userId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        context = PaymentActivity.this;

        imgBack = findViewById(R.id.ach_imgBack);
        webView = findViewById(R.id.ach_webView);
        progressBar = findViewById(R.id.ach_progressBar);

        listeners();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void listeners() {

        Log.e("paymentIntentUrl11", ":" + Constants.PAYMENT_URL);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        CookieManager.getInstance().setAcceptCookie(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        //webView.addJavascriptInterface(new JavaScriptInterface(), "Android");
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        webView.setWebViewClient(new myWebClient());
        webView.loadUrl(Constants.PAYMENT_URL + userId);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBackAlert();
            }
        });
    }

    @SuppressWarnings("deprecation")
    public void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.e("log", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.e("log", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }


    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            startProgress();
            Log.e("onPageStarted", ":" + url);

            //success
            if (url.contains("status_id=1")) {

                intentToResultActivity(true);

                //failure payment

            } else if (url.contains("status_id=3")) {

                intentToResultActivity(false);

                //pending payment
            } else if (url.contains("status_id=2")) {

                intentToResultActivity(false);
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            Log.e("shouldOverride", ":" + url);

            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Log.e("onPageCommit", ":" + url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            dismissProgress();
            Log.e("onPageFinished", ":" + url);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        } else {
            CookieManager.getInstance().removeAllCookie();
            webView.clearCache(true);
            webView.clearHistory();
            clearCookies(context);
            //logoutUser();
        }
        return super.onKeyDown(keyCode, event);
    }


    private void startProgress() {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    private void dismissProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void showBackAlert() {
        CustomAlert customAlert = new CustomAlert(context, "You are sure want to back or cancel this transaction?", "confirm", "cancel", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                CookieManager.getInstance().removeAllCookie();
                webView.clearCache(true);
                webView.clearHistory();
                clearCookies(context);
                closePage(Constants.PAYMENT_DECLINED);
            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
        customAlert.showDialog();
    }

    private void intentToResultActivity(boolean state) {
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra(Constants.PAYMENT_STATE, state ? Constants.PAYMENT_SUCCESS : Constants.PAYMENT_FAILED);
        startActivityForResult(intent, Constants.PAYMENT);
        MyUtils.openOverrideAnimation(true, PaymentActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.PAYMENT:
                closePage(Constants.PAYMENT);
                break;
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        showBackAlert();
    }

    private void closePage(int code) {
        setResult(code);
        finish();
        MyUtils.openOverrideAnimation(false, PaymentActivity.this);
    }


}
