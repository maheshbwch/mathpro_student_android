package com.example.mathpro.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mathpro.R;
import com.example.mathpro.adapters.FilterListAdapter;
import com.example.mathpro.interfaces.WorkTypeListener;
import com.example.mathpro.models.WorkTypes;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.MyUtils;

import java.util.ArrayList;

public class FAQsDetailsActivity extends AppCompatActivity {

    private RecyclerView filterRecycler;
    private ArrayList<WorkTypes> filterArrayList = new ArrayList<>();
    private FilterListAdapter filterListAdapter = null;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs_details);

        filterRecycler = findViewById(R.id.afd_recyclerView);
        imgBack = findViewById(R.id.imgBack);
        filterRecycler.setLayoutManager(new LinearLayoutManager(FAQsDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));
        filterRecycler.setHasFixedSize(true);

        updateFilterListUI();

    }


    private void updateFilterListUI() {

        filterArrayList.clear();
        filterArrayList.add(new WorkTypes(Constants.FREQUENT_QUES, Constants.FREQUENT_QUES, true));
        filterArrayList.add(new WorkTypes(Constants.CREDIT, Constants.CREDIT, false));
        filterArrayList.add(new WorkTypes(Constants.FEATURES, Constants.FEATURES, false));
        filterArrayList.add(new WorkTypes(Constants.GENERAL, Constants.GENERAL, false));
        filterArrayList.add(new WorkTypes(Constants.PRIVACY_POLICY, Constants.PRIVACY_POLICY, false));

        filterListAdapter = new FilterListAdapter(FAQsDetailsActivity.this, filterArrayList);
        filterListAdapter.setOnItemClicked(new WorkTypeListener() {
            @Override
            public void onItemClicked(WorkTypes workTypes) {

            }
        });
        filterRecycler.setAdapter(filterListAdapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, FAQsDetailsActivity.this);
    }
}
