package com.example.mathpro.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mathpro.R;
import com.example.mathpro.utils.Constants;
import com.example.mathpro.utils.MyUtils;


public class ResultActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack, statusImage;
    private TextView statusTxt, statusSubTxt, homePageTxt;
    private String paymentState = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        context = ResultActivity.this;

        init();
    }

    private void init() {
        imgBack = findViewById(R.id.imgBack);
        statusImage = findViewById(R.id.ar_statusImage);
        statusTxt = findViewById(R.id.ar_statusTxt);
        statusSubTxt = findViewById(R.id.ar_statusSubTxt);
        homePageTxt = findViewById(R.id.ar_homePageTxt);

        paymentState = getIntent().getStringExtra(Constants.PAYMENT_STATE);

        listeners();
    }

    private void listeners() {

        if (paymentState.equalsIgnoreCase(Constants.PAYMENT_SUCCESS)) {
            loadUIForSuccess();
        } else if (paymentState.equalsIgnoreCase(Constants.PAYMENT_FAILED)) {
            loadUIForFailure();
        }

        homePageTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }

    private void loadUIForSuccess() {
        statusImage.setImageDrawable(getResources().getDrawable(R.drawable.success_new));
        statusTxt.setText("Your transaction completed successfully");
        statusSubTxt.setText("Please check your balance");
    }

    private void loadUIForFailure() {
        statusImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_red));
        statusTxt.setText("Unable to proceed Your transaction");
        statusSubTxt.setText("Please try after some time");
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        setResult(Constants.PAYMENT);
        finish();
        MyUtils.openOverrideAnimation(false, ResultActivity.this);
    }
}
