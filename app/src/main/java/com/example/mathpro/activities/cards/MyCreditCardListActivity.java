package com.example.mathpro.activities.cards;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mathpro.R;
import com.example.mathpro.adapters.MyCardListAdapter;
import com.example.mathpro.utils.MyUtils;

import java.util.ArrayList;

public class MyCreditCardListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> myCardList = new ArrayList<>();
    private MyCardListAdapter myCardListAdapter = null;

    private TextView txtAddNewCard;
    private ImageView imgBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_credit_card_list);


        txtAddNewCard = findViewById(R.id.addNewCard);

        recyclerView = findViewById(R.id.amc_recyclerView);
        imgBack = findViewById(R.id.imgBack);

        linearLayoutManager = new LinearLayoutManager(MyCreditCardListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        listeners();
    }

    private void listeners() {

        myCardListAdapter = new MyCardListAdapter(MyCreditCardListActivity.this, myCardList);
        recyclerView.setAdapter(myCardListAdapter);


        txtAddNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyCreditCardListActivity.this, AddBankAccount.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, MyCreditCardListActivity.this);

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, MyCreditCardListActivity.this);
    }

}
