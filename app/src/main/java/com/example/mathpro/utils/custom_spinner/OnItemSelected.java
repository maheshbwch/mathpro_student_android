package com.example.mathpro.utils.custom_spinner;

public interface OnItemSelected {
    void onItemSelected(int position);
}
