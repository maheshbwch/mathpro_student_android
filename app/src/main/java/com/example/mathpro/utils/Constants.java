package com.example.mathpro.utils;

public class Constants {

    public static final boolean DEBUG_MODE = false; //TODO SET FALSE BEFORE BUILD APK
    public static boolean IS_INDIAN_CODE = false; //TODO SET FALSE BEFORE BUILD APK
    public static boolean SKIP_OTP = false; //TODO SET FALSE BEFORE BUILD APK

    public static final String API_KEY = "25d55ad283aa400af464c76d713c07ad";
    public static final String APP_NAME = "MATHPRO";
    public static final String PAYMENT_URL = "http://thesispro.bestweb.my/admin/Payment/payment_redirect_url_for_science_and_maths/";

    public static final String INDIA_COUNTRY_CODE= "+91";
    public static final String MALAYSIA_COUNTRY_CODE = "+60";
    public static final String MALAYSIA_COUNTRY_CODE_ID_API = "129";

    //[{"key":"app_id","value":"0","description":"Thesis_pro=0,maths_pro,=2,Science_pro=1,Lecture_app=3"}]
    public static final String APP_ID = "2";
    //[{"key":"user_role_id","value":"3","description":"lecture=2,student=3"}]
    public static final String USER_ROLE_ID = "3";

    public static final int SPLASH_SCREEN_DELAY = 2000; //2 seconds
    public static final String PLATFORM = "android";

    public static final int REGISTRATION = 2012;
    public static final int REGISTRATION_DONE = 2013;
    public static final int REGISTRATION_DECLINED = 2014;
    public static final int FILTER_ACTIVITY = 2015;

    public static final int PICK_IMAGE_FROM_FILES = 1001;
    public static final int CAPTURE_IMAGE_FROM_CAMERA = 1002;
    public static final int WRITE_FINE_LOCATION = 1003;

    public static final int WRITE_STORAGE_FILES_PERMISSION = 2002;
    public static final int WRITE_ATTACHMENT_FILE_PERMISSION = 2006;
    public static final int WRITE_QUESTION_ATTACH_FILE_PERMISSION = 2007;
    public static final int WRITE_NEW_QUES_ATTACH_FILE = 2008;
    public static final int WRITE_STORAGE_CAMERA_PERMISSION = 2006;

    public static final int WRITE_STORAGE_CAMERA_PERMISSION_IC = 2003;
    public static final int WRITE_STORAGE_CAMERA_PERMISSION_PASSPORT = 2004;
    public static final int WRITE_STORAGE_CAMERA_PERMISSION_VISA = 2005;

    public static final int TAKE_IMAGE_FROM_FILES = 2014;
    public static final int TAKE_IMAGE_FROM_CAMERA = 2015;

    public static final int CHANGE_IMAGE_FROM_FILES = 2016;
    public static final int CHANGE_IMAGE_FROM_CAMERA = 2017;
    public static final int PAYMENT = 8080;
    public static final int PAYMENT_DECLINED = 8081;

    public static final String ERROR_DOWNLOAD_FILE = "Download file not found";
    public  static final  String IMAGE_FOLDER = "/MathPRO/";


    public static final int OTP_TIME_OUT = 120;
    public static final int ALERT_TIME_OUT = 10;

    public static final String PREFS_NAME = "pref_name";
    public static final String IS_FROM = "is_from";
    public static final String USER_LOGIN = "user_login";
    public static final String COUNTRY_CODE_ID = "country_code_id";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String PASSWORD = "password";
    public static final String VERIFICATION_ID = "verificationId";
    public static final String PHONE_WITH_CC = "phone_with_cc";

    public static final String PAYMENT_STATE = "payment_state";
    public static final String PAYMENT_SUCCESS = "payment_success";
    public static final String PAYMENT_FAILED = "payment_failed";

    public static final String USER_ID = "userId";
    public static final String CREDIT_BALANCE = "credit_balance";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_MOB = "user_mob";
    public static final String USER_PIC = "user_pic";
    public static final String USER_GENDER = "user_gender";
    public static final String USER_RACE = "user_race";
    public static final String USER_CITY = "user_city";
    public static final String USER_DOB = "user_dob";
    public static final String USER_EMERGENCY_NO = "user_emergency_no";

    public static final String AGE = "age";
    public static final String RACE = "race";
    public static final String EMAIL = "email";
    public static final String PROFILE_IMG_URL = "profileFile";
    public static final String HOME_ADDRESS = "homeAddress";
    public static final String SCHOOL_ADDRESS = "schoolAddress";
    public static final String OFFICE_ADDRESS = "officeAddress";

    public static final String IS_FROM_REGISTER = "is_from_register";
    public static final String IS_FROM_OTP_VERIFY = "is_from_otp_verify";
    public static final String IS_FROM_ACCOUNT_EDIT = "is_from_account_edit";
    public static final String IS_FROM_PROFILE_PIC = "is_from_profile_pic";
    public static final String IS_FROM_LOGIN = "is_from_login";
    public static final String IS_FROM_SIGNUP = "is_from_signup";



    public static final String FCM_TOKEN = "fcm_token";

    public static final String MAID = "maid";
    public static final String CUSTOMER = "customer";

    public static final String IN_TIME = "in";
    public static final String OUT_TIME = "out";

    public static final String ASCENDING = "Asc";
    public static final String DESCENDING = "Desc";

    public static final String SELECTED_DATE = "selectedDate";
    public static final String SERVICE_TYPE = "typeOfService";

    public static final String SERVICE_TYPE_CODE1 = "top_up_details";
    public static final String SERVICE_TYPE_CODE2 = "new";
    public static final String SERVICE_TYPE_CODE3 = "pending";
    public static final String SERVICE_TYPE_CODE4 = "answered";
    public static final String SERVICE_TYPE_CODE5 = "declined";

    public static final String DATE_AND_TIME = "Date";
    public static final String SERVICE_TYPE1 = "+Credit Top Up";
    public static final String SERVICE_TYPE2 = "New";
    public static final String SERVICE_TYPE3 = "Pending";
    public static final String SERVICE_TYPE4 = "Answered";
    public static final String SERVICE_TYPE5 = "Declined";

    public static final String FREQUENT_QUES = "Frequently Asked Questions";
    public static final String CREDIT = "Credits Top Up";
    public static final String FEATURES = "Features";
    public static final String GENERAL = "General";
    public static final String PRIVACY_POLICY = "Privacy Policy";

    public static final String SUCCESS = "1";
    public static final String FAILURE = "0";

    public static final String MAID_LOGIN = "3";

    public static final String DIALOG_SUCCESS = "0";
    public static final String DIALOG_FAILURE = "1";
    public static final String DIALOG_WARNING = "2";
    public static final String DIALOG_GENERAL = "3";

    public static final String REQUESTED = "0";
    public static final String ACCEPTED = "1";
    public static final String DECLINED = "2";
    public static final String COMPLETED = "3";
    public static final String WORKING = "4";
    public static final String FINISHED = "5";

    public static final String DATE_FUTURE = "future_date";
    public static final String DATE_PAST = "past_date";
    public static final String DATE_BIRTH_DATE = "birth_date";
    public static final String DATE_GENERAL = "general";


    public static final String DATE_FORMAT_REVERSE = "yyyy-MM-dd";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String DAY_WITH_DATE_FORMAT = "EEEE, dd-MMM-yyyy";
    public static final String MONTH_NAME_DATE_FORMAT = "MMM";
    public static final String MONTH_NAME_FULL_DATE_FORMAT = "MMMM";

    public static final String IC_TYPE = "ic";
    public static final String PASSPORT_VISA_TYPE = "passport";

    public static final String QUESTIONS_ID = "questions_id";
    public static final String STATUS_QA = "status_qa";


    public static final String APP_VERSION_NAME = "mathpro_app_version_name";
    public static final String APP_VERSION_CODE = "mathpro_app_version_code";
    public static final String APP_IS_FORCE_UPDATE = "mathpro_is_force_update";
    public static final String APP_UPDATE_CONTENT = "mathpro_app_update_content";


    public static final String ERROR_REMOTE_CONFIG = "Unable to proceed further,please try again later";

}
